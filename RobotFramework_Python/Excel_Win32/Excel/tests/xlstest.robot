*** Settings ***
Library             ../libs/ExcelHandler.py
Library               DebugLibrary



*** Test Cases ***


Excel 1
    [tags]          ALV_muistio
    UseExcel       test_file.xlsx
    SetValue       A2       RandomText
    SetValue       B2       1
    SetValue       C2       3
    SetValue       D2       =SUM(B2+C2)
    VerifyCell     D2       4.0
    debug
