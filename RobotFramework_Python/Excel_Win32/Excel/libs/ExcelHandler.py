import time
import os
from functools import wraps
from pathlib import Path
from datetime import date, timedelta
from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn
from robot.utils import timestr_to_secs
import win32com.client as win32


"""Excel library:"""


class ExcelHandler:
    def __init__(self):
        self.path = None
        self.xls = None
        self.book = None
        self.ws = None

    class TimeoutHandler(object):
        err = None
        """ Decorator to handle situations where content
        or file is not ready. This is often needed when file is
        interacting with some third part application."""
        @classmethod
        def timer(cls, fn):
            @wraps(fn)
            def wrapper(*args, **kwargs):
                timeout = timestr_to_secs(kwargs.get('timeout', 3))
                logger.info('timeout: {}'.format(timeout))
                start = time.time()
                while time.time() < timeout + start:
                    try:
                        return fn(*args, **kwargs)
                    except Exception as e:
                        time.sleep(0.5)
                        logger.info('got exception from timeout decorator: {}, fn {}'.format(e, fn))
                        cls.err = e
                raise cls.err
            return wrapper

    """ Available keywords:"""

    @TimeoutHandler.timer
    def use_excel(self, filename, sheet=None, timeout=10):
        """Open Excel file and activate preferred sheet."""
        self.path = self._get_path(filename)
        self._set_open_excel(self.path, timeout=timeout)
        self.pick_sheet(sheet)

    @TimeoutHandler.timer
    def verify_cell(self, cell, expected_value, timeout=3):
        """Verifies that cell contains expected value"""
        cell_value = str(self.ws.Range(cell).Value)
        logger.info(cell_value)
        if cell_value == expected_value:
            return
        raise ValueError(
            'Expected value was {}, but cell value is {}'.format(
                expected_value, cell_value))

    @TimeoutHandler.timer
    def get_value(self, cell, timeout=3):
        """Get value from given cell."""
        cell_value = str(self.ws.Range(cell).Value)
        logger.info(cell_value)
        if cell_value is not "":
            return cell_value
        raise ValueError('Cell {} is empty.')

    def set_value(self, cell, value):
        """Set value to given cell."""
        self.ws.Range(cell).Value = value

    def set_date(self, cell, from_today=0, date_format="T%d.T%m.%Y", starting_zero=False):
        """ Set date value. 0=Today, -1=Yesterday, 1=Tomorrow etc."""
        if not self._str2bool(starting_zero):
            input_date = (date.today()+timedelta(int(from_today))).strftime\
                                        (date_format).replace('T0', '').replace('T', '')
        else:
            input_date = (date.today() + timedelta(int(from_today))).strftime \
                (date_format).replace('T', '')
        self.ws.Range(cell).Value = input_date

    @TimeoutHandler.timer
    def pick_sheet(self, sheet=None):
        """ Pick worksheet to interact with."""
        if sheet:
            self.ws = self.xls.Worksheets[sheet]
        else:
            self.ws = self.xls.ActiveSheet

    def add_sheet(self, name):
        """ Add new worksheet to workbook."""
        self.xls.Worksheets.Add(After=self.xls.ActiveSheet).Name = name
        self.pick_sheet(name)

    def copy_paste(self, cell, target_cell, source_sheet=None, target_sheet=None):
        """ Copy and paste cell value. """
        active = self.ws.Name
        if source_sheet:
            self.pick_sheet(source_sheet)
        self.ws.Range(cell).Copy()
        if target_sheet:
            self.pick_sheet(target_sheet)
        self.ws.Paste(self.ws.Range(target_cell))
        self.pick_sheet(active)

    def get_rows_length(self):
        return self.ws.UsedRange.Rows.Count

    def get_cols_length(self):
        return self.ws.UsedRange.Columns.Count

    def close_excel(self, save=True):
        """Close Excel with or without saving."""
        if self.str2bool(save):
            self.book.Save()
            self.book.Close()
        else:
            self.book.Close(False)
        self.xls.Quit()

    def remove_excel(self):
        """ Remove Excel file from computer."""
        os.remove(self.path)

    """ Helpers: """

    def _set_open_excel(self, filename, timeout):
        """ Open Excel file and instantiate it."""
        xls = win32.gencache.EnsureDispatch('Excel.Application')
        xls.Visible = True
        self.book = xls.Workbooks.Open(filename)
        if not xls:
            raise ValueError('Excel is not ready. Trying again')
        self.xls = xls

    @staticmethod
    def _get_path(filename, proj_dir='files', default='downloads'):
        """Get filepath for Excel-file if not given."""
        if Path(filename).exists():
            return Path(filename)
        file_folder = Path(BuiltIn().get_variable_value(
            '${SUITE SOURCE}')).parent.parent / proj_dir / filename
        dl = Path(os.getenv("HOMEPATH") or os.path.expanduser("~")) / default / filename
        if Path(file_folder).exists():
            return Path(file_folder)
        if Path(dl).exists():
            return Path(dl)
        raise ValueError('Path for {} not found. '
                         'Checked from project folder {} and dl'
                         ' folder {}'.format(filename, proj_dir, default))

    @staticmethod
    def _str2bool(txt):
        """String value to boolean value"""
        if isinstance(txt, str):
            txt = txt.lower()
        return txt in ["true", True]
