

**Library to interact with opened Excel-file.**

This library is used with Oracle adfdi-plugin which handles data transfer between 
Oracle Fusion and Excel-file. Excel needs to be open in order to success and at least by the time
there wasn't any existing RF-library to handle this kind of situation.

**How to use it:**

1. Add library to your project folder and import it in your robot-file
2. Install dependencies if not installed already.


**Dependencies:**

- Win32com.client
- Windows & MSExcel
