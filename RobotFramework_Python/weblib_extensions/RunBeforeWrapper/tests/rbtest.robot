*** Settings ***
Library             QWeb
Library            ../libs/RunBeforeWrapper.py
Suite Setup          SetupBrowser
Suite Teardown       CloseAllBrowsers




*** Test Cases ***

RunBefore Fin
    [tags]      fi
    RunKeywordAndIgnoreError   ClickText    Hyväksyn
    SetRunBefore        TypeText  Haku      Foobar
    VerifyInputValue    Haku      Foobar
    TypeText            Haku      Kalakalle
    VerifyInputValue    Haku      Foobar

RunBefore Eng
    [tags]      eng
    RunKeywordAndIgnoreError   ClickText    Accept
    SetRunBefore        TypeText    Search   Foobar
    VerifyInputValue    Search      Foobar
    TypeText            Search      Kalakalle
    VerifyInputValue    Search      Foobar


*** Keywords ***

SetupBrowser
    OpenBrowser     https://www.google.com     chrome
    SetLibrarySearchOrder   RunBeforeWrapper   QWeb
