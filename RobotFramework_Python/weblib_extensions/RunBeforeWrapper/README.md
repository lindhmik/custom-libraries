

**Simple wrapper for QWeb to add Run Before Keyword functionality.**

Added keywords are decorated in order to change their default behavior. Using keyword
SetRunBefore user can set any keyword to be executed before the actual step.
Feature is very useful for example Oracle Cloud applications (Fusion, PBCS, Siebel) where we want
to be sure that backend processes are finished and loading icon is gone before any new interactions are made.

If this extension is used any extra VerifyNoElement kw:s are not needed in actual test script.

**How to use it:**

1. Add library to your project folder and import it in your robot-file
2. Add keywords you want to be decorated.
3. Use SetLibrarySearchOrder keyword and set RunBeforeWrapper to default
4. Use SetRunBefore kw and define keyword you want to be executed before every step
5. Automate as usual



**Dependencies:**

Nothing special.
