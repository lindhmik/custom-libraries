from functools import wraps
import fnmatch
from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger
from QWeb.internal import util

keyword = None
keyword_args = None


class RunBeforeWrapper:

    def __init__(self):
        self.QWeb = BuiltIn().get_library_instance('QWeb')

    class Deco(object):
        @classmethod
        def run_before(cls, fn):
            @wraps(fn)
            def wrapper(*args, **kwargs):
                if keyword:
                    logger.warn('run before wrapper executed: {}, {}'.format(keyword, keyword_args))
                    exec_func = util.get_callable(keyword)
                    exec_func(*keyword_args)
                return fn(*args, **kwargs)
            return wrapper

    @staticmethod
    def set_run_before(kw, *args):
        r"""
        Set Run Before keyword. When defined this keyword will be
        executed before any other decorated keyword.

        Examples
        --------
        .. code-block:: robotframework

           SetRunBefore  VerifyNoText   FooBar

        Scripts verifies non-existence of text FooBar before every
        other action is executed.
        """
        global keyword
        global keyword_args
        if kw.lower() == 'none':
            keyword = None
        else:
            keyword = kw
            keyword_args = args

    """ Wrapped QWeb keywords: """
    @Deco.run_before
    def type_text(self, *args, **kwargs):
        return self.QWeb.type_text(*args, **kwargs)

    @Deco.run_before
    def click_text(self, *args, **kwargs):
        return self.QWeb.click_text(*args, **kwargs)

    @Deco.run_before
    def click_item(self, *args, **kwargs):
        return self.QWeb.click_item(*args, **kwargs)

    @Deco.run_before
    def verify_text(self, *args, **kwargs):
        return self.QWeb.verify_text(*args, **kwargs)

    @Deco.run_before
    def is_text(self, *args, **kwargs):
        return self.QWeb.is_text(*args, **kwargs)

    @Deco.run_before
    def use_table(self, *args, **kwargs):
        return self.QWeb.use_table(*args, **kwargs)

    @Deco.run_before
    def verify_all(self, *args, **kwargs):
        return self.QWeb.verify_all(*args, **kwargs)

    @Deco.run_before
    def verify_any(self, *args, **kwargs):
        return self.QWeb.verify_any(*args, **kwargs)

    @Deco.run_before
    def verify_input_value(self, *args, **kwargs):
        return self.QWeb.verify_input_value(*args, **kwargs)

    @staticmethod
    @Deco.run_before
    def read_cell_text(locator, timeout=0.5, max_retries=3):
        args = (locator, 'timeout={}'.format(float(timeout)))
        candidates = ['GetInputValue', 'GetSelected', 'GetCellText']
        for i in range(max_retries):
            for c in candidates:
                logger.warn('c = {}, i={}'.format(c, i))
                res, val = BuiltIn().run_keyword_and_ignore_error(c, *args)
                if res == 'PASS':
                    return val
        raise ValueError('Can not read text from cell {}'.format(locator))

    def verify_cell_text(self, locator, expected, timeout=0.5, max_retries=3):
        text = self.read_cell_text(locator, timeout=timeout, max_retries=max_retries)
        if fnmatch.fnmatch(text, expected):
            return
        raise ValueError('{} is not equal to expected {}'.format(text, expected))
