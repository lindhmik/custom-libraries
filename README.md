**This repository is collection of useful automation libraries/library extensions we have done for our customers or ourselves**

Feel free to use and/or modify if there is something suitable for your purposes. Also feel free to share your own ideas and solutions as you probably have something that benefits for some other consultant at some point.



---

** We don't want to be too strict about publishing policies, but we have some common guidelines to follow: **


1. Categorize your library/snippet and put it under right upper level Framework/tool so it's easier to find.
2. There should be at least some kind of documentation/instructions involved. Readme and code comments will do it.
3. If independent library some tests/demo would be nice. It's easier for others to adapt and get the idea when they are able to test it without figuring out what to do.
4. List of dependencies if there is something that might be not that obvious for others.
5. If there are dependencies of external packages add "requirements.txt" file to your folder. For more information see: https://pip.pypa.io/en/stable/reference/pip_install/#requirements-file-format